# Lab8 - measure failure intensity

## Lab

Ok, let's crush all the stuff out of it:
1. Create your fork of the `
Lab7 - Failure intensity
` repo and clone it. [***Here***](https://gitlab.com/sqr-inno/lab7-measure-failure-intensity-s22)
2. For todays lab you will need Apache Benchmark, on linux you can install it using shell command:
```sh
sudo apt install apache2-utils
```
3. And for testing we need some app, just use link from previous lab.
4. To test it you should run command like this one:
```sh
ab -n 20000 -c 100 -m "GET" _url_
```
This means that we are going to send 20000 requests, with 100 of them sending concurrently to the server. We have few lines like `Failed requests` and `Non-2xx responses`, which are exactly failures we're searching here. To measure the exact metric you should use just this formule, where MTTF is time between failures:
`FI = 1 / MTTF`.

## Homework

As a homework you will need to test your service(GetSpec request of InnoDrive from previous labs) and provide the results of your test as a screenshot when failure intensity is equal to the `0.2+-5%`. + provide the calculations for the failure intensity

In this lab, it was necessary to evaluate the failure intensity for the service given to me at https://script.google.com/macros/s/AKfycby4bekTEgvzyqqCKaK8Fd5DO_R-6UIA7IIIpwq59iSOniYZxnV9VNYwK4q84et1j9B38w/exec?service=getSpec&email=d.muhutdinov@innopolis.university.

I used Apache Benchmark for this task. The commands that I used, changing only the number of requests and the concurrency level in them, are following:

Example with 2000 requests and 300 concurrency - ```ab -n 2000 -c 300 -m "GET" "script.google.com/macros/s/AKfycby4bekTEgvzyqqCKaK8Fd5DO_R-6UIA7IIIpwq59iSOniYZxnV9VNYwK4q84et1j9B38w/exec?service=getSpec&email=d.muhutdinov@innopolis.university"```

Example with https, 2000 requests and 100 concurrency - ```abs -n 2000 -c 100 -m "GET" "https://script.google.com/macros/s/AKfycby4bekTEgvzyqqCKaK8Fd5DO_R-6UIA7IIIpwq59iSOniYZxnV9VNYwK4q84et1j9B38w/exec?service=getSpec&email=d.muhutdinov@innopolis.university"```

As a result of running these commands, I was supposed to get the number of requests with an OK code, the number of failed requests, the number of non-2xx requests, and the benchmark execution time. Unfortunately, with none of my machines, I could not get a single OK request. At the same time, in most cases, regardless of the level of concurrency, I received the number of non-2xx requests equal to the number of all requests and very long execution time, as 120000ms for 2000 requests. 
###
![screenshot](bigtime.jpg) 
### 
Assuming that a non-2xx request is considered a failure, then it was not possible for me to measure the Failure Intensity in this way. If in my case I was able to get the ratio of successful requests to failed requests, then I would follow the following steps to get the desired level of concurrency and determine the failure intensity:

1. Empirically choose the right level of concurrency for the number of requests 2000, 10000 and 20000, so that ``` execution time in ms / amount of failed requests  ~= 5```
2. Write down the obtained values using the formulas for MTTF and FI.
With example of 2000 requests, some level of concurrency, with 1500 failed requests and 7800ms of execution time.
- ```MTTF = 7800 ms / 1500 failed requests```
- ```FI = 1/MTTF = 1 / 5.2 ~= 0.192 ~= 0.2```


![screenshot](c8000no2000.jpg)
![screenshot](failed.jpg)
